//
//  TestLayer.h
//  TestConvert
//
//  Created by denizz on 24.03.11.
//  Copyright 2011 dimensino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface TestLayer : CCLayerColor {
    CCLabelTTF *label1;
	CCLabelTTF *label2;
	CCLabelTTF *label3;
	
	CGPoint touchCorrect;
}

+ (id) testLayerWithColor:(ccColor4B)color width:(GLfloat)width height:(GLfloat)height;
- (id) initWithColor:(ccColor4B)color width:(GLfloat)width height:(GLfloat)height;

@end
