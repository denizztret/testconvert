//
//  HelloWorldLayer.m
//  TestConvert
//
//  Created by denizz on 24.03.11.
//  Copyright dimensino 2011. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "TestLayer.h"


@interface HelloWorldLayer (Private)
- (void) labelThatNode:(CCNode *)node;
@end

@implementation HelloWorldLayer

+(CCScene *) scene {
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init {
	if( (self=[super init])) {
		
		[self setIsTouchEnabled:YES];
		
		CGSize size = [[CCDirector sharedDirector] winSize];
//		CGPoint center = ccp( size.width /2 , size.height/2 );
		
		[self labelThatNode:self];
		
		CCLayerColor *back = [CCLayerColor layerWithColor:ccc4(200, 0, 0, 200)];
		[self addChild:back];
		
		infoLayer = [CCLayerColor layerWithColor:ccc4(200, 150, 100, 200) width:300 height:100];
		infoLayer.isRelativeAnchorPoint = YES;
		infoLayer.anchorPoint = ccp(0, 1);
		infoLayer.position = ccp( 0 , size.height );
		[self labelThatNode:infoLayer];
		[self addChild:infoLayer];
		
		infoLabel = [CCLabelTTF labelWithString:@"HELLO" fontName:@"Arial" fontSize:24];
		infoLabel.anchorPoint = ccp(0.5, 0.5);
		infoLabel.position = ccp(infoLayer.contentSize.width/2, infoLayer.contentSize.height/2);
		[infoLayer addChild:infoLabel];
		
		
//		TestLayer *testLayer1 = [TestLayer testLayerWithColor:ccc4(200, 200, 200, 200) width:400 height:400];
//		[self addChild:testLayer1];
//		
//		TestLayer *testLayer2 = [TestLayer testLayerWithColor:ccc4(100, 100, 100, 200) width:200 height:200];
//		testLayer2.position = ccp(0, 0);
//		[testLayer1 addChild:testLayer2];

		
		TestLayer *visual = [TestLayer testLayerWithColor:ccc4(200, 200, 200, 200) width:700 height:700];
		visual.position = ccp(0, 0);
		[self addChild:visual];
		
		TestLayer *parallax = [TestLayer testLayerWithColor:ccc4(200, 200, 100, 200) width:500 height:500];
		parallax.position = ccp(10, 10);
		[visual addChild:parallax];
		
		TestLayer *layer1 = [TestLayer testLayerWithColor:ccc4(100, 100, 200, 150) width:400 height:400];
		layer1.position = ccp(10, 10);
		[parallax addChild:layer1];
		
		TestLayer *frame = [TestLayer testLayerWithColor:ccc4(50, 100, 200, 250) width:200 height:200];
		frame.position = ccp(10, 10);
		[layer1 addChild:frame];
		
//		[self schedule:@selector(update:)];
//		[self scheduleUpdate];
	}
	return self;
}

- (void) dealloc {
	[infoLayer release];
	[infoLabel release];
	[super dealloc];
}

static int val = 0;
-(void) update:(ccTime)dt {	
	val++;
	
//	[infoLabel setString:[NSString stringWithFormat:@"%d", val]];
}

- (void) draw {
	val++;
	[infoLabel setString:[NSString stringWithFormat:@"%d", val]];
}
- (void) labelThatNode:(CCNode *)node {

	NSString *lstr = [NSString stringWithFormat:@"%@ | %@", NSStringFromCGSize(node.contentSize), NSStringFromCGPoint(node.position)];
	CCLabelTTF *label = [CCLabelTTF labelWithString:lstr fontName:@"Arial" fontSize:24];
	label.anchorPoint = ccp(0.5, 1);
	label.position =  ccp( node.contentSize.width /2 , node.contentSize.height );
	[node addChild: label];
}


- (void)handleCameraPan:(NSSet *)touches {
//	NSLog(@"Handling Camera Pan");
	UITouch *touch = [touches anyObject];
	
	CGPoint touchLocation = [touch locationInView: [touch view]];	
	CGPoint prevLocation = [touch previousLocationInView: [touch view]];
	touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
	prevLocation = [[CCDirector sharedDirector] convertToGL: prevLocation];
	
	[infoLabel setString:[NSString stringWithFormat:@"%@", NSStringFromCGPoint(touchLocation)]];
	[infoLabel setColor:ccWHITE];
}
- (void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent *)event {
//	NSLog(@"ccTouchesBegan");
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL: location];  // now in cocos2D coords
	

	[infoLabel setString:[NSString stringWithFormat:@"%@", NSStringFromCGPoint(location)]];
	[infoLabel setColor:ccYELLOW];
}
- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
//	NSLog(@"Current World Pos x: %f \t y: %f", [self position].x, [self position].y);
	//NOT HANDLING A DISC AND SO CAN HANDLE ZOOMING
	switch ([[event allTouches] count]) {
		case 1:
			// do scroll
			[self handleCameraPan:touches];
			break;
		case 2:
			break;
	}
	
}


@end
