//
//  HelloWorldLayer.h
//  TestConvert
//
//  Created by denizz on 24.03.11.
//  Copyright dimensino 2011. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer {
	
	CCLayerColor *infoLayer;
	CCLabelTTF *infoLabel;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
