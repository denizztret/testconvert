//
//  AppDelegate.h
//  TestConvert
//
//  Created by denizz on 24.03.11.
//  Copyright dimensino 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
