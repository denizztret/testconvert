//
//  TestLayer.m
//  TestConvert
//
//  Created by denizz on 24.03.11.
//  Copyright 2011 dimensino. All rights reserved.
//

#import "TestLayer.h"
/*
 Test commit 2
 */

@implementation TestLayer

+ (id) testLayerWithColor:(ccColor4B)color width:(GLfloat)width height:(GLfloat)height {
	return [[[TestLayer alloc] initWithColor:color width:width height:height] autorelease];
}
- (id) initWithColor:(ccColor4B)color width:(GLfloat)width height:(GLfloat)height {
	self = [super initWithColor:color width:width height:height];
    if (self) {
		
		CGSize size = [[CCDirector sharedDirector] winSize];
		CGPoint center = ccp( size.width/2 , size.height/2 );
		
//		self.anchorPoint = ccp(0.5, 0.5);
//		self.isRelativeAnchorPoint = YES;
		self.position = center;
		
		
        label1 = [CCLabelTTF labelWithString:@"label1" fontName:@"Arial" fontSize:24];
		label1.anchorPoint = ccp(0.5, 1);
		label1.position =  ccp( self.contentSize.width/2 , self.contentSize.height );
		[label1 setColor:ccWHITE];
		[self addChild:label1];
		
		label2 = [CCLabelTTF labelWithString:@"label2" fontName:@"Arial" fontSize:24];
		label2.anchorPoint = ccp(0.5, 1);
		label2.position =  ccp( self.contentSize.width/2 , label1.position.y - label1.contentSize.height);
		[label2 setColor:ccWHITE];
		[self addChild:label2];
		
		label3 = [CCLabelTTF labelWithString:@"label3" fontName:@"Arial" fontSize:24];
		label3.anchorPoint = ccp(0.5, 1);
		label3.position =  ccp( self.contentSize.width/2 , label2.position.y - label2.contentSize.height );
		[label3 setColor:ccWHITE];
		[self addChild:label3];
		
		
		[self setIsTouchEnabled:NO];
		
    }
    return self;
}

- (void) dealloc {
    [label1 release];
	[label2 release];
	[label3 release];
	
    [super dealloc];
}

- (void) onEnter {
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
	[super onEnter];
	
	[self schedule:@selector(update:)];
}
- (void) onExit {
	[[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
	[super onExit];
	
	[self unscheduleAllSelectors];
}	

- (void) update:(ccTime)dt {	
	
	CGPoint point = self.position;
	CGPoint zeroPoint = CGPointZero;
	
	NSString *str1 = [NSString stringWithFormat:@"%@", NSStringFromCGPoint(point)];
	NSString *str2 = [NSString stringWithFormat:@"%@", NSStringFromCGPoint([self convertToNodeSpace:zeroPoint])];
	NSString *str3 = [NSString stringWithFormat:@"%@", NSStringFromCGPoint([self.parent convertToWorldSpace:point])];
	
	[label1 setString:[NSString stringWithFormat:@"%@", str1]];
	[label2 setString:[NSString stringWithFormat:@"%@", str2]];
	[label3 setString:[NSString stringWithFormat:@"%@", str3]];
}

#pragma mark -

- (BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {

	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL: location];  // now in cocos2D coords
	
	touchCorrect = ccpSub(self.position, location);
	
	CGPoint pos = [self convertToNodeSpace:location];
	
	CGRect frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);	
	
	if (!CGRectContainsPoint(frame, pos)) {
		return NO;
	}
	
	return YES;
}
- (void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL: location];
	
	self.position = ccpAdd(location, touchCorrect);
	
}


@end
